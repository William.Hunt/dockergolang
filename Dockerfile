# LICENSE UPL 1.0
#
# Copyright (c) 2014-2015 Oracle and/or its affiliates. All rights reserved.
#
# ORACLE DOCKERFILES PROJECT
# --------------------------
#
# Dockerfile template for Oracle Instant Client
#
# REQUIRED FILES TO BUILD THIS IMAGE
# ==================================
# 
# From http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html
#  Download the following three RPMs:
#    - oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
#    - oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
#    - oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm 
#
# HOW TO BUILD THIS IMAGE
# -----------------------
# Put all downloaded files in the same directory as this Dockerfile
# Run: 
#      $ docker build --pull -t oracle/instantclient:12.2.0.1 .
#
#
FROM oraclelinux:7-slim

RUN yum update -y
RUN yum install -y pkgconfig && \
    yum install -y unzip && \
    yum install -y tar && \
    yum install -y gzip && \
    yum install -y git && \
    yum install -y gcc && \
    yum install -y wget && \
    yum install -y rsync

RUN wget https://dl.google.com/go/go1.13.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.13.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin

ADD oracle-instantclient*.rpm /tmp/

RUN  yum -y install /tmp/oracle-instantclient*.rpm && \
     rm -rf /var/cache/yum && \
     rm -f /tmp/oracle-instantclient*.rpm && \
     echo /usr/lib/oracle/12.2/client64/lib > /etc/ld.so.conf.d/oracle-instantclient12.2.conf && \
     ldconfig

ENV PATH=$PATH:/usr/lib/oracle/12.2/client64/bin
ENV PKG_CONFIG_PATH=/usr/lib/oracle/12.2/client64/bin

COPY oci8.pc /usr/lib/oracle/12.2/client64/bin/oci8.pc

ENV LD_LIBRARY_PATH=/usr/lib:/usr/local/lib:/usr/lib/oracle/12.2/client64/bin

CMD ["sqlplus", "-v"]
